define(function(require, exports, module) {
    var Engine = require('famous/core/Engine');
	var Surface = require('famous/core/Surface');
	var Transform = require('famous/core/Transform');
	var StateModifier = require('famous/modifiers/StateModifier');
	var Transitionable = require('famous/transitions/Transitionable');
	var ImageSurface = require("famous/surfaces/ImageSurface");
	var SpringTransition = require('famous/transitions/SnapTransition');
	var Easing = require('famous/transitions/Easing');
	var TextareaSurface = require('famous/surfaces/TextareaSurface');
	var RenderController = require("famous/views/RenderController");
	var View = require("famous/core/View");
	
	Transitionable.registerMethod('spring', SpringTransition);
	
	var mainContext = Engine.createContext();
	
	var renderController = new RenderController();
	
	mainContext.add(renderController);
	
	var secondView = new View();
		
	var browserWidth = window.innerWidth;
	var browserHeight = window.innerHeight;
	
	var Surface1 = new Surface({
	  properties: {
	    backgroundColor: 'rgb(102, 102, 255)'
	  }
	});
	var Surface2 = new Surface({
	  properties: {
	    backgroundColor: 'rgb(000, 153, 153)'
	  }
	});
	var Surface3 = new Surface({
	  properties: {
	    backgroundColor: 'rgb(64, 64, 64)'
	  }
	});
	var input1 = new TextareaSurface({
	  size: [0.95 * browserWidth, 0.88 * browserHeight],
	  properties: {
	    backgroundColor: 'rgb(64, 64, 64)',
	    boxShadow: '0px 0px 50px #fff',
	    borderRadius: '10px',
	    outlineStyle: 'inherit',
	    color: 'white',
	    textAlign: 'left',
	    classes: ['input']
	  }
	});
	var image = new ImageSurface({
        size: [100, 100],
        radius: '100px',
        classes: ['imageRound'],
        content: "assets/img/Dog.jpg"
    });
    
    var noteIcon = new ImageSurface({
    	size: [40, 40],
    	classes: ['imageCurve'],
    	content: "assets/img/noteIcon.jpg"
    });
    
    var socialIcon = new ImageSurface({
    	size: [40, 40],
    	classes: ['imageCurve'],
    	content: "assets/img/socialIcon.jpg"
    });
    
    var reminderIcon = new ImageSurface({
    	size: [40, 40],
    	classes: ['imageCurve'],
    	content: "assets/img/reminderIcon.jpg"
    });
    
    var backIcon = new ImageSurface({
    	size: [40, 40],
    	classes: ['imageCurve'],
    	content: "assets/img/backIcon.jpg"
    });
    
    var modifier1 = new StateModifier({
    	size: [browserWidth	, 70]
    });
	var stateModifier1 = new StateModifier({
	  transform: Transform.translate(0,350,0)
	});
	var stateModifier2 = new StateModifier({
	  transform: Transform.translate(0,420,0)
	});
	var stateModifier3 = new StateModifier({
	  transform: Transform.translate(0,490,0)
	});
	var inputModifier1 = new StateModifier({
		transform: Transform.translate(0.025 * browserWidth, 0.1 * browserHeight, 0)
	});
	
	var modifierImage = new StateModifier({
		opacity: 0
	});
	var imageModifier = new StateModifier({
	  	transform: Transform.translate(10,5,0)
    });
    
    var noteModifier = new StateModifier({
    	transform: Transform.translate(browserWidth - 60,84,0)
    });
    
    var socialModifier = new StateModifier({
    	transform: Transform.translate(browserWidth - 60,155,0)
    });
    
    var reminderModifier = new StateModifier({
    	transform: Transform.translate(browserWidth - 60,226,0)
    });
    
    var backModifier = new StateModifier({
    	transform: Transform.translate(10, 5, 0)
    }); 
    var viewTemp = mainContext.add(modifier1);
    viewTemp.add(stateModifier1).add(Surface1);
    viewTemp.add(stateModifier2).add(Surface2);
    viewTemp.add(stateModifier3).add(Surface3);
	
	secondView.add(inputModifier1).add(input1);
	
	var spring = {
	  method: 'spring',
	  period: 800,
	  dampingRatio: 1
	};
	
	var openSurface = {
	  method: 'spring',
	  period: 200,
	  dampingRatio: 1
	};
	
	var fadeImage = function (modif) {
		modif.setOpacity(1, {
    		duration: 400,
    		curve: Easing.inQuad
    	});
	};
	
	var addImages = function () {
		var tempImageView = mainContext.add(modifierImage);
		tempImageView.add(imageModifier).add(image);
		tempImageView.add(noteModifier).add(noteIcon);
		tempImageView.add(socialModifier).add(socialIcon);
		tempImageView.add(reminderModifier).add(reminderIcon);
    	fadeImage(modifierImage);
	};
	
	var addBackImage = function () {
		secondView.add(backModifier).add(backIcon);
		fadeImage(backModifier);
		backIcon.setClasses(['bringtofront']);
		input1.setClasses(['bringtofront']);
		backModifier.setTransform(Transform.translate(10, 5, 0));
		renderController.show(secondView);
	};
	modifier1.setTransform(
		Transform.translate(0,-280,0), spring, addImages
	);
	var activeSurface = null;
	var activeModifier = null; 
	
	var manageSurfaces = function (surface, modifier, posY) {
		activeSurface = surface;
		activeModifier = modifier;
		activeTransform = modifier.getTransform();
		modifier.setSize([browserWidth, 70]); // Temp Fix: Need to understand why underlay modifiers are not taking parent size
		surface.setClasses(['bringtofront']);
		modifier.setTransform(Transform.translate(0, 280, 0), openSurface, addBackImage);
		modifier.setSize([browserWidth, browserHeight], openSurface);
	};
	noteIcon.on('click', function () {
		manageSurfaces(Surface1, stateModifier1);
	});
	socialIcon.on('click', function () {
		manageSurfaces(Surface2, stateModifier2);
	});
	reminderIcon.on('click', function () {
		manageSurfaces(Surface3, stateModifier3);
	});
	backIcon.on('click', function () {
		renderController.hide(secondView, function () {
				activeModifier.setTransform(activeTransform, openSurface);
				activeModifier.setSize([browserWidth, 70], openSurface, function() {
				activeSurface.setClasses(['normalPos']);
			});
		});
	});
});
